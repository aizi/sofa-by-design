import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { Post } from '../models/post';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  postList: Post[];
  form: FormGroup;
  messagesList: AngularFireList<any>;


  constructor(
    private postService: PostService, private router: Router, private fb: FormBuilder, private af: AngularFireModule,
    private db: AngularFireDatabase
  ) {
    this.createForm();

   }

  ngOnInit() {
    return this.postService.getPosts()
      .snapshotChanges().subscribe(item => {
        this.postList = [];
        item.forEach(element => {
          const x = element.payload.toJSON();
          x['$key'] = element.key;
          this.postList.push(x as Post);
        });
      });
  }

  onEdit(post: Post) {
    this.postService.selectedPost = Object.assign({}, post);
  }

  onDelete($key: string) {
    if (confirm('Are you sure you want to delete it?')) {
      this.postService.deletePost($key);
    }
  }
  createForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      sujet: ['', Validators.required],
      message: ['', Validators.required],

    });
  }
  onSubmit() {
    const {name, email, sujet , message} = this.form.value;
    const date = Date();
    const html = `
      <div>From: ${name}</div>
      <div>Email: <a href="mailto:${email}">${email}</a></div>
      <div>Date: ${date}</div>
      <div>Sujet: ${sujet}</div>
      <div>Message: ${message}</div>
    `;
    const formRequest = { name, email, message, date, html };
    this.db.list('/messages').push(formRequest);

    this.form.reset();
  }
}