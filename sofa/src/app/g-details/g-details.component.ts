
import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

// Firebase
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
// service
import { ProductService } from '../services/product.service';
@Component({
  selector: 'app-g-details',
  templateUrl: './g-details.component.html',
  styleUrls: ['./g-details.component.scss']
})
export class GDetailsComponent implements OnInit {
  productList: Product[];
  private sub: any;
  object: any;

  constructor(private produitService: ProductService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
     this.object = params;
     console.log(params);
   });



    return this.produitService.getProducts()
      .snapshotChanges().subscribe(item => {
        this.productList = [];
        item.forEach(element => {
          const x = element.payload.toJSON();
          x['$key'] = element.key;
          this.productList.push(x as Product);
        });
      });
  }
  // onEdit(product: Product) {
  //   this.router.navigate(['nouveau-P']);
  //   this.produitService.selectedProduct = Object.assign({}, product);
  // }
  onDelete($key: string) {
    if (confirm('Are you sure you want to delete it?')) {
      this.produitService.deleteProduct($key);
    }
  }

  addTocart(product: Product ) {
    const object = this.produitService.selectedProduct = Object.assign({}, product);
    console.log(object);
    this.router.navigate(['panier', object]);
    console.log(object);


  }

}