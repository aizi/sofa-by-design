import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GDetailsComponent } from './g-details.component';

describe('GDetailsComponent', () => {
  let component: GDetailsComponent;
  let fixture: ComponentFixture<GDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
