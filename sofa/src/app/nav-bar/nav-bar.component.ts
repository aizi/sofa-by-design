import { Component, OnInit } from '@angular/core';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from '@firebase/util';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  user: Observable<firebase.User>;
public isLoggedIn: Boolean = false;
private email: string;

constructor( private fire: AngularFireAuthModule , public afAuth: AngularFireAuth  , public router: Router ) {
  const status = localStorage.getItem('isLoggedIn');
  console.log(status);

  if (status === 'true') {
  this.isLoggedIn = true;
  } else {
    this.isLoggedIn = false;
  }
      //   firebase.auth().onAuthStateChanged(function(user) {
    // if (user) {
    //   this.isLoggedIn = true;
    // } else {
    //   this.isLoggedIn = false;
    //   this.router.navigate(['/connexion']);
    // }

    //  });
  }
    ngOnInit() {
    }
    logout() {
       this.afAuth.auth.signOut();
      this.isLoggedIn = false;
      localStorage.setItem('isLoggedIn', 'false');
      this.router.navigate(['/connexion']);
    }

}
