import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { GalerieComponent } from './galerie/galerie.component';
import { AccueilComponent } from './accueil/accueil.component';

import { GDetailsComponent } from './g-details/g-details.component';
import { AboutUsComponent } from './about-us/about-us.component';

import { ConnexionComponent } from './authentification/connexion/connexion.component';
import { InscriptionComponent } from './authentification/inscription/inscription.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './products/product/product.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { PanierComponent } from './panier/panier.component';
import { GalerieDetailsComponent } from './galerie/galerie-details/galerie-details.component';
import { ModuleWithProviders } from '@angular/core';
import { CartComponent } from './gestion/cart/cart.component';
import { ProductsAllComponent } from './gestion/products-all/products-all.component';
export const routes: Routes = [
  {path: '', redirectTo: 'Accueil', pathMatch: 'full'},
  {path: 'connexion', component: ConnexionComponent},
  {path: 'inscription', component: InscriptionComponent},
  {path: 'galerie', component: GalerieComponent},
  {path: 'g-details', component: GDetailsComponent},
  {path: 'Accueil', component: AccueilComponent},
  {path: 'products', component: ProductsComponent},
  {path: 'product', component: ProductComponent},
  {path: 'product-list', component: ProductListComponent},
  {path: 'about-us', component: AboutUsComponent},
  {path: 'panier', component: PanierComponent},
  {path: 'galerie-details', component: GalerieDetailsComponent},
  { path: 'products-cart', component: ProductsAllComponent },
  { path: 'cart', component: CartComponent },


];
export const router: ModuleWithProviders = RouterModule.forRoot(routes);


