import { Component } from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})

export class CarComponent {
  title = 'app';
  url = '';
  listItems: any;
  constructor() {
    this.listItems = [
      {
        name: 'Products',
        link: '#/products'
      },
      {
        name: 'Cart',
        link: '#/cart'
      },
    ];
  }
  add(title, url) {
    if (title !== '' && url !== '') {
      url = 'http://' + url;
      this.listItems.push({
        name: title,
        link: url,
        isNew: true
      });
      this.title = '';
      this.url = '';
    }
  }
}
