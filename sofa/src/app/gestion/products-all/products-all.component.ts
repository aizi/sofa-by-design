import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { Product } from '../../models/product';

@Component({
  selector: 'app-products-all',
  templateUrl: './products-all.component.html',
  styleUrls: ['./products-all.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsAllComponent implements OnInit {
  // products: Product[];

   products: any;
  cartProducts: any;

  constructor(private router: Router , public productService: ProductService) { }

  ngOnInit() {
    let data = localStorage.getItem('cart');
    if (data !== null) {
      this.cartProducts = JSON.parse(data);
    } else {
      this.cartProducts = [];
    }

// ======================================================
return this.productService.getProductsCart()
      .snapshotChanges().subscribe(item => {
        this.products = [];
        item.forEach(element => {
          const x = element.payload.toJSON();
          x['$key'] = element.key;
          this.products.push(x as Product);
        });
      });

  }
  onEdit(product: Product) {
    const object = this.productService.selectedProduct = Object.assign({}, product);
    console.log(object);
    this.router.navigate(['g-details', object]);
    console.log(object);
  }


  addToCart(index) {
    let product = this.products[index];
    let cartData = [];
    const data = localStorage.getItem('cart');
    if (data !== null) {
      cartData = JSON.parse(data);
    }
    cartData.push(product);
    this.updateCartData(cartData);
    localStorage.setItem('cart', JSON.stringify(cartData));
    this.products[index].isAdded = true;
  }
  updateCartData(cartData) {
    this.cartProducts = cartData;
  }
  goToCart() {
    this.router.navigate(['/cart']);
  }
}
