import { Component, OnInit } from '@angular/core';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';



@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {
  errorMessage: string = '';
  email: string = '';
  password: string = '';
  constructor( private fire: AngularFireAuthModule , public afAuth: AngularFireAuth  , private router: Router ) { }

  ngOnInit() {
  }

  login() {
    this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password)
      .then(value => {
        console.log('Nice, it worked!');
        localStorage.setItem('isLoggedIn', 'true' );
        this.router.navigateByUrl('/Accueil');
      })
      .catch(err => {
        console.log('Something went wrong: ', err.message);
      });
  }
  inscription() {
    this.router.navigateByUrl('/inscription');

  }

}
