import { Component, OnInit } from '@angular/core';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {
  errorMessage: string = '';
  email: string = '';
  password: string = '';
  constructor(private fire: AngularFireAuthModule , public afAuth: AngularFireAuth  , private router: Router ) { }

  ngOnInit() {
  }
  register() {
    this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.password)
      .then(value => {
        console.log('Nice, it worked!');
        this.router.navigateByUrl('/Accueil');
      })
      .catch(err => {
        console.log('Something went wrong: ', err.message);
      });
  }
  connexion() {
    this.router.navigateByUrl('/connexion');

  }
}
