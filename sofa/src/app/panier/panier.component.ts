import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { ProductService } from './../services/product.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

// Firebase
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
// Class
import { Product } from './../models/product';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { CartService } from '../services/cart.service';

// ========================================================================

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {
  private sub: any;
  public products: Product[] = [];
  object: any;

  constructor(
    public productService: ProductService,
    protected localStorage: LocalStorage,
    private router: Router,
    private route: ActivatedRoute,
    private cartService: CartService
  ) {}

  ngOnInit() {
    // ========================================get localstorage
    this.products = this.cartService.getProducts();
    console.log(this.products);
    // ======================================================affichage
  }
  /*totalCart() {
    const priceProduct = this.object.price;
    const quantity = 1;
    const sum = 0;
    this.products.forEach(function(item) {
 sum += item.quantity * item.priceProduct;
    });
    return sum;
}*/
onDelete(object) {
  if (confirm('Are you sure you want to delete it?')) {
    console.log(object);
    this.cartService.deleteProduct(object);
  }
}

}
