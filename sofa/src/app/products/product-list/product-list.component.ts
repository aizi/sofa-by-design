import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// model
import { Product } from '../../models/product';

// service
import { ProductService } from '../../services/product.service';

import { LocalStorage } from '@ngx-pwa/local-storage';
import { CartService } from '../../services/cart.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  productList: Product[];

  constructor(
    public productService: ProductService, private cartService: CartService, protected localStorage: LocalStorage, private router: Router
  ) { }

  ngOnInit() {
    return this.productService.getProducts()
      .snapshotChanges().subscribe(item => {
        this.productList = [];
        item.forEach(element => {
          const x = element.payload.toJSON();
          x['$key'] = element.key;
          this.productList.push(x as Product);
        });
      });
  }

  onEdit(product: Product) {
    const object = this.productService.selectedProduct = Object.assign({}, product);
    console.log(object);
    this.router.navigate(['g-details', object]);
    console.log(object);
  }

  onDelete($key: string) {
    if (confirm('Are you sure you want to delete it?')) {
      this.productService.deleteProduct($key);
    }
  }
  addProductToCart(product) {
    // const product: User = { firstName: 'Henri', lastName: 'Bergson' };

    this.cartService.addProduct(product);
    // this.shoppingCartService.addItem(product, 1);
  }

}


