import { Injectable } from '@angular/core';

// Firebase
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

// Model
import { Post } from '../models/post';

@Injectable()
export class PostService {

  postList: AngularFireList<any>;
  selectedPost: Post = new Post();

  constructor(private firebase: AngularFireDatabase) { }

  getPosts() {
    return this.postList = this.firebase.list('posts');
  }

  insertPost(post: Post) {
      this.postList.push({
      sujet: post.sujet,
      sousTitre: post.sousTitre,
      image: post.image,
      contenu: post.contenu
    });
  }

  updatePost(post: Post) {
    this.postList.update(post.$key, {
      sujet: post.sujet,
      sousTitre: post.sousTitre,
      image: post.image,
      contenu: post.contenu
    });
  }

  deletePost($key: string) {
    this.postList.remove($key);
  }
}