import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { Subject } from 'rxjs/Subject';
// import { DH_NOT_SUITABLE_GENERATOR } from 'constants';

@Injectable({
  providedIn: 'root'
})
export class CartService {
cartSubject = new Subject<any[]>();
products: Product[] = [];

  constructor() {
    this.init();
  }
  emitCartSubject() {
    this.cartSubject.next(this.products.slice());
    }
  init() {
    this.products = JSON.parse(localStorage.getItem('products')) || [];
    this.emitCartSubject();

  }

  addProduct(product) {
    this.products.push(product);
    this.saveToLocalStorage();
    this.emitCartSubject();
  }

  getProducts() {
    return this.products.slice();
  }

  saveToLocalStorage() {
    localStorage.setItem('products', JSON.stringify(this.products));
    this.emitCartSubject();

  }

  deleteProduct(toDelete) {
    const products = JSON.parse(localStorage.getItem('products'));
    const test = products.filter(product => product.$key !== toDelete.$key);
    console.log (test);
    this.getProducts();
    this.emitCartSubject();

  }
}
