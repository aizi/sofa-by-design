import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/';
import { Http } from '@angular/http';
import {map} from 'rxjs/operators';
import { Router } from '@angular/router';
import {Response, Headers, RequestOptions} from '@angular/http';
import { Detail } from '../models/detail';

// import 'rxjs/add/operator/map';

@Injectable()
export class GalerieService {
    selected: Detail = new Detail();

    constructor( private http: Http, private router: Router) {

    }

    search(motCle: string, size: number, currentPage: number) {

        return this.http.get('https://pixabay.com/api/?key=5832566-81dc7429a63c86e3b707d0429&q='
        + motCle + '&per_page=' + size + '&page=' + currentPage)
        .pipe(map(resp => {
        return resp.json();
        }));
       }
       onDetailImage(p) {
        this.router.navigate(['/galerieDetails'], { queryParams: { myImage: p } });
       }
}
