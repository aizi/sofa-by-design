import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalerieDetailsComponent } from './galerie-details.component';

describe('GalerieDetailsComponent', () => {
  let component: GalerieDetailsComponent;
  let fixture: ComponentFixture<GalerieDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalerieDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalerieDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
