import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-galerie-details',
  templateUrl: './galerie-details.component.html',
  styleUrls: ['./galerie-details.component.scss']
})
export class GalerieDetailsComponent implements OnInit {
  object: any;
private sub: any;
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.object = params;
      console.log(params);
  // const object = +this.route.snapshot.params['object'];
  });
}
}
