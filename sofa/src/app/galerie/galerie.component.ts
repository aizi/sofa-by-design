import { Http } from '@angular/http';
import {map} from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { GalerieService } from '../services/galerie.service';
import { Router } from '@angular/router';
 import { HttpClientModule , HttpClient} from '@angular/common/http';
 import { Detail } from '../models/detail';

import 'rxjs/add/operator/map';
@Component({
  selector: 'app-galerie',
  templateUrl: './galerie.component.html',
  styleUrls: ['./galerie.component.scss']
})
export class GalerieComponent implements OnInit {
  currentPage: number = 1; // n de page affiché sur l'écran
  size: number = 8; // nombre de page ( pagination)
  pages: Array<number> = []; // déclaration d'un tableau de pages
  motCle: string = '';
  totalPages: number = 0;
  pagePhoto: any;
  constructor(private http: Http, private router: Router, private galerieService: GalerieService) {
   }

  ngOnInit() {}
  // la pagination

 goToPage(i) {
  this.currentPage = i + 1; // la page suivante
  this.onSearch({motCle: this.motCle});
  // faire appelle a la recherche  pour la page suivante je transmi un objet que je recupére en html motCle
}
onSearch(dataForm) {
this.galerieService.search(dataForm.motCle, dataForm.size, dataForm.currentPage)
.subscribe(data => {
  this.pagePhoto = data; // asynchrone
  this.totalPages = data.totalHits / this.size; // selon les données json de API
  // if ( data.totalHits % this.size !== 0) ++this.totalPages; // si le reste est différent de 1 (le reste) on ajoute 1
// this.pages = new Array(this.totalPages); // création du tableau des pages
   this.pages = new Array(20); // création du tableau des pages
}, err => {
  console.log(err);
});
}

lookDetail(p: Detail ) {
  const object = this.galerieService.selected = Object.assign({}, p);
  console.log(object);
  this.router.navigate(['galerie-details', object]);
  console.log(object.previewURL);


}



  }
