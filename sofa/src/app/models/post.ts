export class Post {
    $key: string;
    sujet: string;
    sousTitre: string;
    image: any;
    contenu: any;
}