export class Detail {
    comments: any;
    previewURL: any;
    tags: string;
    userImageURL: any;
    user: any;
    likes: number;
}
