export class Product {
    $key: string;
    name: string;
    category: string;
    description: string;
    price: number;
    image: URL;
}
