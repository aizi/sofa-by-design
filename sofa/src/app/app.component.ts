import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
// import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import { getLocaleDateFormat } from '@angular/common';
import * as $ from 'jquery';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  posts: any[];
  lastUpdate = new Date();
  ngOnInit() {

  }
  getDate() {
    return this.lastUpdate;
  }

  constructor() {

  }
}