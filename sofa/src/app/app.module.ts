import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
// import { select } from '@angular-redux/core';
// import ngRedux from 'ng-redux';
// import { NgReduxModule, NgRedux } from '@angular-redux/core';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import { getLocaleDateFormat } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { GalerieComponent } from './galerie/galerie.component';
// import 'rxjs/add/operator/map';
import { HttpModule } from '@angular/http';
// import { map } from 'rxjs/operators';
import { GalerieService } from './services/galerie.service';
import { routes } from './app-routing.module';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';
import { PostService } from './services/post.service';

// components
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './products/product/product.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { ConnexionComponent } from './authentification/connexion/connexion.component';
import { InscriptionComponent } from './authentification/inscription/inscription.component';
// service
import { ProductService } from './services/product.service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
// import { LocalStorage } from '@ngx-pwa/local-storage';
import { CarComponent } from './gestion/car.component';
import {ReactiveFormsModule} from '@angular/forms';
// service======================================================

// =====================================================================
// Toastr
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartComponent } from './gestion/cart/cart.component';
import { ProductsAllComponent } from './gestion/products-all/products-all.component';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { GDetailsComponent } from './g-details/g-details.component';
import { AccueilComponent } from './accueil/accueil.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from './footer/footer.component';
import { PanierComponent } from './panier/panier.component';
import { GalerieDetailsComponent } from './galerie/galerie-details/galerie-details.component';
// import { ToastrModule } from 'ngx-toastr';
@NgModule({
  declarations: [
    AppComponent,
    GalerieComponent,
    CartComponent,
    ProductsAllComponent,
    GDetailsComponent,
    CarComponent,
    ProductsComponent,
    ProductComponent,
    ProductListComponent,
    AccueilComponent,
    AboutUsComponent,
    NavBarComponent,
    FooterComponent,
    ConnexionComponent,
    InscriptionComponent,
    PanierComponent,
    GalerieDetailsComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    ReactiveFormsModule,
    // LocalStorage
  ],
  providers: [
     PostService ,
     ProductService,
     GalerieService,
     {provide: LocationStrategy, useClass: HashLocationStrategy}

    ],
  bootstrap: [AppComponent]
})
export class AppModule {}

